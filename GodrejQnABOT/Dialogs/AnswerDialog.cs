﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GodrejQnABOT.Dialogs
{
    [Serializable]
    public class AnswerDialog : IDialog<object>
    {
        public class QnAMakerResult
        {
            [JsonProperty(PropertyName = "answers")]
            public List<Result> Answers { get; set; }
        }

        public class Result
        {
            [JsonProperty(PropertyName = "answer")]
            public string Answer { get; set; }

            [JsonProperty(PropertyName = "questions")]
            public List<string> Questions { get; set; }

            [JsonProperty(PropertyName = "score")]
            public double Score { get; set; }
        }


        public Task StartAsync(IDialogContext context)
        {
            context.Wait(QuestionReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task QuestionReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;

            await context.PostAsync(GetAnswer(activity.Text));
        }

        //private string GetAnswer(string query)
        //{
        //    string responseString = string.Empty;

        //    var knowledgebaseId = Convert.ToString(WebConfigurationManager.AppSettings["KNOWLEDGE_BASE_ID"], CultureInfo.InvariantCulture);

        //    //Build the URI
        //    var builder = new UriBuilder(string.Format(Convert.ToString(WebConfigurationManager.AppSettings["QNA_SERVICE_URL"], CultureInfo.InvariantCulture)));

        //    //Add the question as part of the body
        //    var postBody = string.Format("{{\"question\": \"<{0}>\"}}", query);
        //  //  { "question":"<Your question>"}

        //    //Send the POST request
        //    using (WebClient client = new WebClient())
        //    {
        //        //Set the encoding to UTF8
        //        client.Encoding = System.Text.Encoding.UTF8;

        //        //Add the subscription key header
        //        var qnamakerSubscriptionKey = Convert.ToString(WebConfigurationManager.AppSettings["SUBSCRIPTION_KEY"], CultureInfo.InvariantCulture);
        //        client.Headers.Add("Ocp-Apim-Subscription-Key", qnamakerSubscriptionKey);
        //        client.Headers.Add("Content-Type", "application/json");
        //        responseString = client.UploadString(builder.Uri, postBody);
        //    }
        //    QnAMakerResult result = JsonConvert.DeserializeObject<QnAMakerResult>(responseString);
        //    return result.Answers[0].Answer;
        //}
     
        public string GetAnswer(string query)
        {
            string responseString = string.Empty;
            var knowledgebaseId = "49355419-0a72-4b1b-b63a-6ebb8107ef43"; // Use knowledge base id created.
            var qnamakerSubscriptionKey = "ba6f17bc-f150-48e8-8cc6-db0d3a7a796d"; //Use subscription key assigned to you.
                                              //Build the URI
          //  Uri qnamakerUriBase = new Uri("https://godrejqna.azurewebsites.net/qnamaker/");
            var builder = new UriBuilder(@"https://godrejqna.azurewebsites.net/qnamaker/knowledgebases/" + knowledgebaseId + "/generateAnswer");
          //var builder= "https://godrejqna.azurewebsites.net/qnamaker/knowledgebases/49355419-0a72-4b1b-b63a-6ebb8107ef43/generateAnswer";

            //Add the question as part of the body
            var postBody = "{\"question\": \"" + query + "\"}";

            //Send the POST request
            using (WebClient client = new WebClient())
            {
                //Set the encoding to UTF8
                client.Encoding = System.Text.Encoding.UTF8;

                //Add the subscription key header
                client.Headers.Add("Ocp-Apim-Subscription-Key", qnamakerSubscriptionKey);
                client.Headers.Add("Content-Type", "application/json");
               // JObject json = JObject.Parse(postBody);
               // var path = json.ToString();
                //responseString = client.UploadString(builder, postBody);
               responseString = client.UploadString(builder.Uri, postBody);
                var response = JsonConvert.DeserializeObject<Result>(responseString);
                if (response.Score > 10.0)
                {
                    return response.Answer;
                }
                else
                {
                    return "Sorry, I don't have an answer. Please mail admin@application.com";
                }

            }
        }
    }
}