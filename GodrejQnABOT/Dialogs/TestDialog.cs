﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;

namespace GodrejQnABOT.Dialogs
{
    [Serializable]
    public class TestDialog : IDialog<object>
    {
        // NOTE: Replace this with a valid host name.
        static string host = "https://godrejqna.azurewebsites.net";

        // NOTE: Replace this with a valid endpoint key.
        // This is not your subscription key.
        // To get your endpoint keys, call the GET /endpointkeys method.
        static string endpoint_key = "ba6f17bc-f150-48e8-8cc6-db0d3a7a796d";

        // NOTE: Replace this with a valid knowledge base ID.
        // Make sure you have published the knowledge base with the
        // POST /knowledgebases/{knowledge base ID} method.
        static string kb = "49355419-0a72-4b1b-b63a-6ebb8107ef43";

        static string service = "/qnamaker";
        static string method = "/knowledgebases/" + kb + "/generateAnswer/";

        static string question = @"
{
    'question': 'Name?',
    'top': 3
}
";
        public class QnAMakerResult
        {
            [JsonProperty(PropertyName = "answers")]
            public List<Result> Answers { get; set; }
        }

        public class Result
        {
            [JsonProperty(PropertyName = "answer")]
            public string Answer { get; set; }

            [JsonProperty(PropertyName = "questions")]
            public List<string> Questions { get; set; }

            [JsonProperty(PropertyName = "score")]
            public double Score { get; set; }
        }
        public Task StartAsync(IDialogContext context)
        {
           
            context.Wait(MessageReceivedAsyncStart);

            return Task.CompletedTask;
        }
        private async Task MessageReceivedAsyncStart(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("Wellcome To Godrej FAQ");
            context.Wait(MessageReceivedAsync);
        }
        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;

            var uri = host + service + method;

            var query = activity.Text;
           // var postBody = @"{'question': '"+ query + "'}";
            var postBody = "{\"question\": \"" + query + "\"}";

            var responseString = await Post(uri, postBody);
            var response = JsonConvert.DeserializeObject<QnAMakerResult>(responseString);
            if (response != null)
            {
                foreach (var v in response.Answers)
                {
                    Result resultObj = v;
                    await context.PostAsync(resultObj.Answer);
                }
            }
            else
            {
                await context.PostAsync("Sorry, I don't have an answer. Please mail admin@application.com");
            }
            context.Wait(MessageReceivedAsync);
        }

    async static Task<string> Post(string uri, string body)
    {
        using (var client = new HttpClient())
        using (var request = new HttpRequestMessage())
        {
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri(uri);
            request.Content = new StringContent(body, Encoding.UTF8, "application/json");
            request.Headers.Add("Authorization", "EndpointKey " + endpoint_key);

            var response = await client.SendAsync(request);
            return await response.Content.ReadAsStringAsync();
        }
    }

    async static void GetAnswers()
    {
        var uri = host + service + method;
        Console.WriteLine("Calling " + uri + ".");
        var response = await Post(uri, question);
        Console.WriteLine(response);
        Console.WriteLine("Press any key to continue.");
    }
}
}



